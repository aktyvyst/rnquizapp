import { StatusBar } from "expo-status-bar";
import React from "react";
import { View } from "react-native";
import QuestionView from "./src/views/QuestionView";
const App = (props) => {
  const mockedQuestion = {
    num: 1,
    question: "Wie heisst der Bürgermeister von Wesel?",
    options: ["Dieter", "Peter", "Data", "Esel"],
    answer: 3
  };
  return (
    <View style={{ flex: 1 }}>
      <QuestionView question={mockedQuestion} />
      <StatusBar style="auto" />
    </View>
  );
};
export default App;
