import React, { useState } from "react";
import { SafeAreaView, View, Text, ScrollView } from "react-native";
import { Badge, Button } from "react-native-elements";
import { appStyles } from "../styles";
import AnswerButton from "../components/AnswerButton";
const QuestionView = (props) => {
  const [selectedAnswer, setSelectedAnswer] = useState(-1);
  const { question } = props;

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ ...appStyles.container, flex: 1 }}>
        <View style={appStyles.questionContainer}>
          <Badge
            value={question.num}
            badgeStyle={{ width: 30, height: 30, borderRadius: 15 }}
            containerStyle={{
              flex: 1,
              marginTop: 8,
              height: 30,
              width: 30
            }}
          />
          <Text style={appStyles.questionHeadline} numberOfLines={2}>
            {question.question}
          </Text>
        </View>
        <ScrollView>
          {question.options.map((answer, i) => {
            const disabled = selectedAnswer > -1;
            const isCorrect =
              selectedAnswer == i && selectedAnswer === question.answer;
            const disabledStyling = selectedAnswer === i;
            return (
              <AnswerButton
                key={`answer-${i}`}
                questionIndex={i}
                selectAnswerHandler={setSelectedAnswer}
                title={answer}
                disabled={disabled}
                isCorrect={isCorrect}
                disabledStyling={disabledStyling}
              />
            );
          })}
          <Button
            title="Weiter"
            type="clear"
            onPress={() => alert("Nächste Frage bitte ;)")}
            disabled={!(selectedAnswer > -1)}
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default QuestionView;
