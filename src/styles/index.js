export const appColors = {
  main: "#4196F6",
  secondary: "#14143D",
  error: "#CB016F",
  green: "#1F990E",
  gray: "#777777"
};
export const appStyles = {
  safeArea: { flex: 1 },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: "flex-start",
    justifyContent: "center",
    padding: 25
  },
  questionContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    height: "auto"
    // borderWidth: 1
    // alignContent: "flex-start"
  },
  standardText: {
    fontSize: 15,
    marginLeft: 25,
    marginRight: 25
  },
  countdown: {
    fontSize: 15,
    color: appColors.main,
    fontWeight: "bold",
    marginLeft: 10,
    marginTop: 4
  },
  standardInput: {
    fontSize: 15,
    margin: 20
  },
  boldText: {
    fontSize: 15,
    paddingLeft: 25,
    paddingRight: 25,
    fontWeight: "bold"
  },
  questionHeadline: {
    fontSize: 14,
    color: appColors.secondary,
    flex: 5,
    height: 60,
    paddingTop: 5,
    fontWeight: "bold"
    // paddingTop: 25
  },
  button: {
    backgroundColor: appColors.main,
    width: "100%",
    alignSelf: "center",
    margin: 25,
    borderRadius: 8
  },
  quizButton: {
    alignSelf: "center",
    margin: 25,
    width: "90%",
    borderRadius: 8,
    borderWidth: 1,
    borderColor: appColors.main,
    backgroundColor: "white"
  },
  quizButtonTitle: {
    fontSize: 12,
    color: appColors.secondary,
    textAlign: "left"
  },
  buttonText: {
    width: "100%",
    textAlign: "center"
  }
};
