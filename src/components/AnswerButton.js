import React from "react";
import { Button } from "react-native-elements";
import { appStyles, appColors } from "../styles";

const AnswerButton = (props) => {
  const {
    questionIndex,
    selectAnswerHandler,
    title,
    disabled,
    disabledStyling,
    isCorrect
  } = props;

  return (
    <Button
      buttonStyle={{
        ...appStyles.quizButton
      }}
      title={title}
      titleStyle={appStyles.quizButtonTitle}
      disabledStyle={{
        backgroundColor: isCorrect ? "green" : !disabledStyling ? "none" : "red"
      }}
      disabledTitleStyle={{
        color: disabledStyling ? "white" : appColors.gray
      }}
      onPress={() => selectAnswerHandler(questionIndex)}
      disabled={disabled}
    />
  );
};

export default AnswerButton;
